using Microsoft.EntityFrameworkCore;
using StoreMVC.DataAccess;


var builder = WebApplication.CreateBuilder(args);

// Connection String to connect with SQL SERVER
const string CONNECTIONNAME = "StoreDB";
var connectionString = builder.Configuration.GetConnectionString(CONNECTIONNAME);

// Add services to the container.
builder.Services.AddControllersWithViews();


// Add DbContexts to Services
builder.Services.AddDbContext<StoreContext>(x => x.UseSqlServer(connectionString));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
