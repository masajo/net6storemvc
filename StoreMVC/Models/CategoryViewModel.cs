﻿using Microsoft.AspNetCore.Mvc.Rendering;
using StoreMVC.Models.DataModels;

namespace StoreMVC.Models
{
    public class CategoryViewModel
    {
        public Category? Category { get; set; }

        public IEnumerable<SelectListItem>? ProductsCombo { get; set; }

        public List<int>? SelectedProducts { get; set; }

    }
}
