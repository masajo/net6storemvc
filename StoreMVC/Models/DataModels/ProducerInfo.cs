﻿using System.ComponentModel.DataAnnotations;

namespace StoreMVC.Models.DataModels
{
    public class ProducerInfo
    {
        [Key]
        public int ProducerId { get; set; }

        public DateTime CreationDate { get; set; }

        public string Nationality { get; set; } = string.Empty;

        public int NumberEmployees { get; set; }

    }
}
