﻿namespace StoreMVC.Models.DataModels
{
    public class ProductPresentation
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;
    }
}
