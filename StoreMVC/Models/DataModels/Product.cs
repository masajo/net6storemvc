﻿namespace StoreMVC.Models.DataModels
{
    public class Product
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public double Price { get; set; }

        public ICollection<ProductPresentation>? Presentations { get; set; }

        public ICollection<Category>? Categories { get; set; }
    }
}
