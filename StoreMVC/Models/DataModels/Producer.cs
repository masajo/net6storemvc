﻿using System.ComponentModel.DataAnnotations;

namespace StoreMVC.Models.DataModels
{
    public class Producer
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string CIF { get; set; } = string.Empty;

        public ProducerInfo? Info { get; set; }

    }
}
