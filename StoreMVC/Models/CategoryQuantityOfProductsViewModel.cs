﻿namespace StoreMVC.Models
{
    public class CategoryQuantityOfProductsViewModel
    {
        public int Id { get; set; }

        public string? Name { get; set; }
        public int productsQuantity { get; set; }
    }
}
