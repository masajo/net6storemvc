﻿using StoreMVC.Models.DataModels;

namespace StoreMVC.Models
{
    public class ProductsViewModel
    {
        public IEnumerable<Product> Products { get; set; } = Enumerable.Empty<Product>();

        public string? SearchName { get; set; } = string.Empty;

        public double? SearchPrice { get; set; }
    }
}
