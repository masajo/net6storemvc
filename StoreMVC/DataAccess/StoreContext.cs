﻿using Microsoft.EntityFrameworkCore;
using StoreMVC.Models;
using StoreMVC.Models.DataModels;

namespace StoreMVC.DataAccess
{
    public class StoreContext : DbContext
    {

        public StoreContext(DbContextOptions<StoreContext> options): base(options) { }


        // DbSets of Store Data Base
        public DbSet<Product>? Products { get; set; }
        public DbSet<Category>? Categories { get; set; }
        public DbSet<ProductPresentation>? ProductPresentations { get; set; }

        public DbSet<Producer>? Producers { get; set; }

        public DbSet<ProducerInfo>? ProducersInfo { get; set; }

        public DbSet<CategoryQuantityOfProductsViewModel> CategoryQuantityOfProductsViewModel { get; set; }

    }
}
