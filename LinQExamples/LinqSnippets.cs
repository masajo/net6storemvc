﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinQExamples
{

    public class Employee
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Salary { get; set; }
        public string? Email { get; set; }

    }

    public class Enterprise
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public Employee[]? Employees { get; set; };

    }


    public class LinqSnippets
    {

        StringBuilder sbCars = new StringBuilder();

        static public void BasicLinq()
        {

            string[] cars =
            {
                "VW Golf",
                "VW California",
                "Audi A3",
                "Audi A4",
                "Fiat Punto",
                "Seat León"
            };

            // 1. SELECT * and save it into a list
            var carList = from car in cars select car;

            StringBuilder sbCars = new StringBuilder();

            // Using StringBuilder
            foreach (string car in carList)
            {
                sbCars.Append('-' + car + '\n');
            }

            Console.WriteLine(sbCars.ToString());

            // Clean the string Builder
            sbCars.Clear();

            // 2. WHERE SELECT
            var audiList = from car in cars where car.Contains("Audi") select car;

            // Using StringBuilder
            foreach (string audi in audiList)
            {
                sbCars.Append('-' + audi + '\n');
            }

            Console.WriteLine(sbCars.ToString());

            // 3. ORDER BY (ascending)
            var orderdCarList = from car in cars orderby car ascending select car;

        }

        static public void LinqNumbers()
        {

            List<int> numbersList = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // Each number is multiplied by 3
            // Take all numbers but 9
            // Order By Ascending
            var processedList =
                numbersList
                .Select(num => num * 3)
                .Where(num => num != 9)
                .OrderBy(num => num);

        }


        static public void Search()
        {

            List<string> textList = new List<string>
            {
                "a", "bx", "c", "d", "e", "cj", "f", "c"
            };

            // First element of a list
            var first = textList.First();

            // First element that is c
            var cText = textList.First(text => text.Equals("c"));

            // First element that contains j
            var jText = textList.First(text => text.Contains("j"));

            // First Element with "z" or Default
            // Returns the first element which contains Z or default value for string
            var firstOrDefaultText = textList.FirstOrDefault(text => text.Contains("z"));

            // Last Element with "z" or Default
            // Returns the last element which contains Z or default value for string
            var lastOrDefaultText = textList.LastOrDefault(text => text.Contains("z"));

            // SINGLE
            // Single ensures the return of a unique value
            var uniqueElement = textList.Single();
            var uniqueOrDefaultElement = textList.SingleOrDefault();


            // EXCEPT
            int[] evenNumbers = { 0, 2, 4, 6, 8 };
            int[] otherEvenNumbers = { 0, 2, 6 };

            // Obtain { 4, 8 }
            var myEvenList = evenNumbers.Except(otherEvenNumbers);


        }


        static public void MultipleSelect()
        {

            // SELECT MANY
            string[] myOpinions = { "Opinion 1,text 1", "Opinion 2,text 2", "Opinion 3,text 3" };

            var mySelection = myOpinions.SelectMany(opinion => opinion.Split(','));


            // Using Enterprise & Employee classes
            var enterprises = new[]
            {
                new Enterprise()
                {
                    Id = 1,
                    Name = "Enterprise 1",
                    Employees = new []
                    {
                        new Employee
                        {
                            Id = 1,
                            Name = "Martín",
                            Email = "martin@imaginagorup.com",
                            Salary = 2000
                        },
                        new Employee
                        {
                            Id = 2,
                            Name = "Oscar",
                            Email = "oscar@imaginagorup.com",
                            Salary = 1000
                        },
                        new Employee
                        {
                            Id = 3,
                            Name = "Diego",
                            Email = "diego@imaginagorup.com",
                            Salary = 3000
                        }
                    }
                },
                new Enterprise()
                {
                    Id = 2,
                    Name = "Enterprise 2",
                    Employees = new []
                    {
                        new Employee
                        {
                            Id = 4,
                            Name = "Eric",
                            Email = "eric@imaginagorup.com",
                            Salary = 200
                        },
                        new Employee
                        {
                            Id = 5,
                            Name = "Anabel",
                            Email = "anabel@imaginagorup.com",
                            Salary = 5000
                        },
                        new Employee
                        {
                            Id = 6,
                            Name = "Juan",
                            Email = "juan@imaginagorup.com",
                            Salary = 100
                        }
                    }
                }
            };

            // Obtain all Employees of all enterprises
            var employeeList = enterprises.SelectMany(enterprise => enterprise.Employees);

            // Know if any list is empty
            bool hasEnterprises = enterprises.Any(); // true

            bool haveEmployees = enterprises.Any(enterprise => enterprise.Employees.Any()); // true

            bool haveEmployeeWithSalaryMoreThan1000 = 
                enterprises.Any(enterprise => 
                    enterprise.Employees.Any(employee => employee.Salary > 1000)); // true

        }

        static public void linqCollections()
        {

            var firstList = new List<string>() { "a", "b", "c" };
            var secondList = new List<string>() { "a", "c", "d" };

            // INNER JOIN
            var commonResult = from element in firstList
                               join secondElement in secondList
                               on element equals secondElement
                               select new { element, secondElement };

            var commonResult2 = firstList.Join(
                secondList,
                element => element,
                secondElement => secondElement,
                (element, secondElement) => new { element, secondElement }
                );
 

            // OUTER JOIN - LEFT
            var leftOuterJoin = from element in firstList
                                join secondElement in secondList
                                on element equals secondElement
                                into temporalList
                                from temporalElement in temporalList.DefaultIfEmpty()
                                where element != temporalElement
                                select new { Element = element };

            var leftOuterJoin2 = from element in firstList
                                 from secondElement in secondList.Where(s => s == element).DefaultIfEmpty()
                                 select new { Element = element, SecondElement = secondElement };


            // OUTER JOIN - RIGHT
            var rightOuterJoin = from secondElement in secondList
                                 join element in firstList
                                 on secondElement equals element
                                 into temporalList
                                 from temporalElement in temporalList.DefaultIfEmpty()
                                 where secondElement != temporalElement
                                 select new { Element = secondElement };
            // UNION
            var unionList = leftOuterJoin.Union(rightOuterJoin);


        }


        static public void SkipTakeLinq()
        {

            var myList = new[]
            {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
            };

            
            // SKIP

            var skipTwoFirstValues = myList.Skip(2); // { 3, ... }

            var skipTwoLastValues = myList.SkipLast(2); // { 1, 2, 3, 4, 5, 6, 7, 8 }

            var skipWhile = myList.SkipWhile(s => s < 4); // { 4, 5, 6, 7, 8, 9, 10 }

            // TAKE
            var takeFirstTwoValues = myList.Take(2); // { 1, 2 }

            var takeTwoLastValues = myList.TakeLast(2); // { 9, 10 }

            var takeWhile = myList.TakeWhile(s => s < 4); // { 1, 2, 3 }


            var skipOneAndTakeTwoValues = myList.Skip(1).Take(2); // { 2, 3 }

            var emptyList = myList.Take(0); // { }


        }


        // Variables in LINQ

        // ZIP in LINQ

        // Repeat in LINQ

        // All in LINQ

        // AGGREGATE in LINQ

        // DISCTINCT in LINQ

        // GROUP BY in LINQ

    }
}